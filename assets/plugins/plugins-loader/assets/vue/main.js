import Vue from 'vue';

import VueDraggable from 'vue-draggable';
Vue.use(VueDraggable);

import Notifications from 'vue-notification';
Vue.use(Notifications);

//Template
import pluginsLoader from './PluginsLoader';

// console.log(window);
// window.onload = () => {
window.addEventListener('DOMContentLoaded', () => {
    let container = document.querySelector('#p_loader');
    console.log(container);
    if (container) {
        setTimeout(() => {
            new Vue({
                el: container,
                render: h => h(pluginsLoader)
            });
        }, 0);
    }
});