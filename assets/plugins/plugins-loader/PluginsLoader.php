<?php
/*
 * Plugin Name: Plugins Loader
 * Version: 3.0
 * Plugin URI: https://svitsoft.com/
 * Description: Plugins favorite installation.
 * Author: Svitsoft
 * Author URI: https://svitsoft.com/
 */

if ( !defined( 'ABSPATH' ) ) exit;


define( 'SP_PATH', plugin_dir_path( __FILE__ ));
define( 'SP_URL', plugin_dir_url(__FILE__) );

require_once SP_PATH . '/vendor/autoload.php';


use HaydenPierce\ClassFinder\ClassFinder;

class PluginsLoader {

    private static $basedir;
    private static $namespace = 'PluginsLoaderUI';
	private static $pluginSlug = 'plugins-loader';

    public function __construct() {
        self::$basedir = plugin_dir_path( __FILE__ ) . 'inc/classes/';

        self::cc_autoload();

	    self::checkUpdates();
    }

    private static function cc_autoload() {

        $namespaces = self::getDefinedNamespaces();
        foreach ( $namespaces as $namespace => $path ) {

            $clear = substr( $namespace, 0, strlen( $namespace ) - 1 );

            ClassFinder::setAppRoot( SP_PATH );
            $level = error_reporting( E_ERROR );

            $classes = ClassFinder::getClassesInNamespace( $clear );
            error_reporting( $level );

            foreach ( $classes as $class ) {
                new $class();
            }
        }
    }

    private static function getDefinedNamespaces() {
        $composerJsonPath = dirname( __FILE__ ) . '/composer.json';

        $composerConfig = json_decode( file_get_contents( $composerJsonPath ) );

        $psr4 = "psr-4";
        return (array)$composerConfig->autoload->$psr4;
    }

	private static function checkUpdates() {
		$slug = self::$pluginSlug;
		$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
			"http://services.svitsoft.com/wp-update-server/?action=get_metadata&slug={$slug}",
			__FILE__,
			$slug
		);
	}
}

new PluginsLoader();