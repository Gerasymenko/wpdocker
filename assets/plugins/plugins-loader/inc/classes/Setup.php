<?php
/**
 * Created by PhpStorm.
 * User: gerasart
 * Date: 1/23/2019
 * Time: 2:01 PM
 */


namespace PluginsLoader;

class Setup {

    public function __construct() {
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin' ) );
    }

    public function enqueue_admin() {
//        $options = get_option( 'current_plugins' );
        wp_enqueue_script( 'plugins-loader', SP_URL . 'dist/js/bundle.js' );

        wp_localize_script( 'jquery', 'pl_loader_url', SP_URL );
//        if ($options) {
//            wp_localize_script( 'jquery', 'current_plugins', get_option( 'current_plugins' ) );
//        }
    }


    /* Debug functions */
    public static function debug( $var ) {
        echo "<pre>";
        var_dump( $var );
        echo "</pre>";
    }

}