<?php


namespace PluginsLoader\helpers;


use PluginsLoader\Traits\PluginsHelper;

class Installer {

	use PluginsHelper;

	const WP_REPO_REGEX = '|^http[s]?://wordpress\.org/(?:extend/)?plugins/|';

	const IS_URL_REGEX = '|^http[s]?://|';

	public $default_path = '';

	public $strings = [
		'oops'       => 'Error',
		'installing' => 'Installing',
		'updating'   => 'Updating',
	];

	public $plugins = [];

	public function __construct( $plugins = false ) {
		if ( $plugins ) {
			$this->plugins = self::formatPluginList( $plugins );
		}

		$this->default_path = SP_PATH . 'inc/plugins/';
	}

	public function installPlugins( $plugins ) {
		$this->plugins = self::formatPluginList( $plugins );

		$install_type  = 'install';
		$results       = [];

		foreach ( $this->plugins as $plugin ) {
			$res = $this->install( $plugin, $install_type );

			$results[ $plugin['slug'] ] = [
				'plugin' => $plugin,
				'result' => $res,
			];
		}

		return $results;
	}

	public function deactivatePlugins($plugins) {
		$slugs = array_column($plugins, 'slug');
		$list = [];
		foreach ($slugs as $slug) {
			$list[] = $this->_get_plugin_basename_from_slug($slug);
		}

		\deactivate_plugins($list);

		return true;
	}

	public function install( $plugin, $update = false ) {
		// All plugin information will be stored in an array for processing.
//		$slug = $this->sanitize_key( urldecode( $_GET['plugin'] ) );
		$install_type = false;

		ob_start();

		$exist = self::pluginExist( $plugin );
		if ( $exist && $update ) {
			$install_type = 'update';
		} elseif( !$exist ) {
			$install_type = 'install';
		} elseif ( $exist ) {
			$path = $this->_get_plugin_basename_from_slug( $plugin['slug'] );
			$name = plugin_basename( trim( $path ) );

			if ( !is_plugin_active( $name ) ) {
				activate_plugin($name, '', false, true);

				return 'plugin-activated';
			} else {
				return 'plugin-exist';
			}
		}

		$plugin = $this->getPluginInfo($plugin);
		$skin_args = $this->createPluginSkin( $plugin, $install_type );

		if ( ! class_exists( 'Plugin_Upgrader', false ) ) {
			require_once ABSPATH . 'wp-admin/includes/class-wp-upgrader.php';
		}

		if ( 'update' === $install_type ) {
			$skin_args['plugin'] = $plugin['file_path'];
			$skin                = new \Plugin_Upgrader_Skin( $skin_args );
		} else {
			$skin = new \Plugin_Installer_Skin( $skin_args );
		}

		// Create a new instance of Plugin_Upgrader.
		$upgrader = new \Plugin_Upgrader( $skin );

		// Perform the action and install the plugin from the $source urldecode().
//		add_filter( 'upgrader_source_selection', array( $this, 'maybe_adjust_source_dir' ), 1, 3 );
		$source = $this->get_download_url( $plugin );
		if ( 'update' === $install_type ) {
			// Inject our info into the update transient.
			$plugin['source'] = $source;

			$to_inject = [
				$plugin['slug'] => $plugin
			];

			$this->inject_update_info( $to_inject );

			$upgrader->upgrade( $plugin['file_path'] );
		} else {
			$upgrader->install( $source );
		}

		if (ob_get_length()) {
			ob_end_clean();
		}

		$plugin_activate = $upgrader->plugin_info();
		if ( $plugin_activate ) {
			activate_plugin( $plugin_activate );

			return 'plugin-installed';
		} else {
			return 'plugin-rejected';
		}
	}

	public static function pluginExist($plugin) {
		$installed_plugins = self::getPluginsList();
		$plugins_slug = array_column( $installed_plugins['all'], 'slug' );

		return in_array( $plugin['slug'], $plugins_slug );
	}

	/**
	 * Get plugin full info
	 * @param $plugin
	 *
	 * @return mixed
	 */
	public function getPluginInfo( $plugin ) {
		$plugin['source'] = isset( $plugin['source'] ) && ! empty( $plugin['source'] )
			? $plugin['source'] : 'repo';

		$plugin['file_path'] = $this->_get_plugin_basename_from_slug( $plugin['slug'] );

		$plugin['source_type'] = $this->get_plugin_source_type( $plugin['source'] );

		return $plugin;
	}


	/**
	 * Prep variables for Plugin_Installer_Skin class.
	 * @param $plugin
	 * @param string $install_type
	 *
	 * @return array
	 */
	public function createPluginSkin( $plugin, $install_type = 'install' ) {
		$slug = $plugin['slug'];

		$api = ( 'repo' === $plugin['source_type'] ) ? $this->get_plugins_api( $slug ) : null;
		$api = ( false !== $api ) ? $api : null;

		$url = add_query_arg(
			array(
				'action' => $install_type . '-plugin',
				'plugin' => urlencode( $slug ),
			),
			'update.php'
		);

		$title     = ( 'update' === $install_type ) ? $this->strings['updating'] : $this->strings['installing'];
		$skin_args = array(
			'type'   => ( 'bundled' !== $plugin['source_type'] ) ? 'web' : 'upload',
			'title'  => sprintf( $title, $plugin['name'] ),
			'url'    => esc_url_raw( $url ),
			'nonce'  => $install_type . '-plugin_' . $slug,
			'plugin' => '',
			'api'    => $api,
			'extra'  => [
				'slug' => $slug,
			],
		);
		unset( $title );

		return $skin_args;
	}


	/**
	 * Inject information into the 'update_plugins' site transient as WP checks that before running an update.
	 *
	 * @param array $plugins The plugin information for the plugins which are to be updated.
	 *
	 * @since 2.5.0
	 *
	 */
	public function inject_update_info( $plugins ) {
		$repo_updates = get_site_transient( 'update_plugins' );

		if ( ! is_object( $repo_updates ) ) {
			$repo_updates = new \stdClass;
		}

		foreach ( $plugins as $slug => $plugin ) {
			$file_path = $plugin['file_path'];

			if ( empty( $repo_updates->response[ $file_path ] ) ) {
				$repo_updates->response[ $file_path ] = new \stdClass;
			}

			// We only really need to set package, but let's do all we can in case WP changes something.
			$repo_updates->response[ $file_path ]->slug        = $slug;
			$repo_updates->response[ $file_path ]->plugin      = $file_path;
			$repo_updates->response[ $file_path ]->new_version = $plugin['version'];
			$repo_updates->response[ $file_path ]->package     = $plugin['source'];
			if ( empty( $repo_updates->response[ $file_path ]->url ) && ! empty( $plugin['external_url'] ) ) {
				$repo_updates->response[ $file_path ]->url = $plugin['external_url'];
			}
		}

		set_site_transient( 'update_plugins', $repo_updates );
	}

	/**
	 * Determine what type of source the plugin comes from.
	 *
	 * @param string $source The source of the plugin as provided, either empty (= WP repo), a file path
	 *                       (= bundled) or an external URL.
	 *
	 * @return string 'repo', 'external', or 'bundled'
	 * @since 2.5.0
	 *
	 */
	protected function get_plugin_source_type( $source ) {
		if ( 'repo' === $source || preg_match( self::WP_REPO_REGEX, $source ) ) {
			return 'repo';
		} elseif ( preg_match( self::IS_URL_REGEX, $source ) ) {
			return 'external';
		} else {
			return 'bundled';
		}
	}

	/**
	 * Helper function to extract the file path of the plugin file from the
	 * plugin slug, if the plugin is installed.
	 *
	 * @param string $slug Plugin slug (typically folder name) as provided by the developer.
	 *
	 * @return string Either file path for plugin if installed, or just the plugin slug.
	 * @since 2.0.0
	 *
	 */
	protected function _get_plugin_basename_from_slug( $slug ) {
		$keys = array_keys( $this->get_plugins() );

		foreach ( $keys as $key ) {
			if ( preg_match( '|^' . $slug . '/|', $key ) ) {
				return $key;
			}
		}

		return $slug;
	}

	/**
	 * Wrapper around the core WP get_plugins function, making sure it's actually available.
	 *
	 * @param string $plugin_folder Optional. Relative path to single plugin folder.
	 *
	 * @return array Array of installed plugins with plugin information.
	 * @since 2.5.0
	 *
	 */
	public function get_plugins( $plugin_folder = '' ) {
		if ( ! function_exists( 'get_plugins' ) ) {
			require_once ABSPATH . 'wp-admin/includes/plugin.php';
		}

		return get_plugins( $plugin_folder );
	}

	/**
	 * Sanitizes a string key.
	 *
	 * Near duplicate of WP Core `sanitize_key()`. The difference is that uppercase characters *are*
	 * allowed, so as not to break upgrade paths from non-standard bundled plugins using uppercase
	 * characters in the plugin directory path/slug. Silly them.
	 *
	 * @see https://developer.wordpress.org/reference/hooks/sanitize_key/
	 *
	 * @since 2.5.0
	 *
	 * @param string $key String key.
	 *
	 * @return string Sanitized key
	 */
	public function sanitize_key( $key ) {
		$raw_key = $key;
		$key     = preg_replace( '`[^A-Za-z0-9_-]`', '', $key );

		/**
		 * Filter a sanitized key string.
		 *
		 * @param string $key Sanitized key.
		 * @param string $raw_key The key prior to sanitization.
		 *
		 * @since 2.5.0
		 *
		 */
		return apply_filters( 'tgmpa_sanitize_key', $key, $raw_key );
	}

	/**
	 * Retrieve the download URL for a package.
	 *
	 * @param string $plugin Plugin.
	 *
	 * @return string Plugin download URL or path to local file or empty string if undetermined.
	 * @since 2.5.0
	 *
	 */
	public function get_download_url( $plugin ) {
		$dl_source = '';

		switch ( $plugin['source_type'] ) {
			case 'repo':
				return $this->get_wp_repo_download_url( $plugin['slug'] );
			case 'external':
				return $plugin['source'];
			case 'bundled':
				return $this->default_path . $plugin['source'];
		}

		return $dl_source; // Should never happen.
	}

	/**
	 * Retrieve the download URL for a WP repo package.
	 *
	 * @param string $slug Plugin slug.
	 *
	 * @return string Plugin download URL.
	 * @since 2.5.0
	 *
	 */
	protected function get_wp_repo_download_url( $slug ) {
		$source = '';
		$api    = $this->get_plugins_api( $slug );

		if ( false !== $api && isset( $api->download_link ) ) {
			$source = $api->download_link;
		}

		return $source;
	}

	/**
	 * Try to grab information from WordPress API.
	 *
	 * @param string $slug Plugin slug.
	 *
	 * @return object Plugins_api response object on success, WP_Error on failure.
	 * @since 2.5.0
	 *
	 */
	protected function get_plugins_api( $slug ) {
		static $api = array(); // Cache received responses.

//		if ( ! isset( $api[ $slug ] ) ) {
		if ( ! function_exists( 'plugins_api' ) ) {
			require_once ABSPATH . 'wp-admin/includes/plugin-install.php';
		}

		$response = plugins_api( 'plugin_information', array( 'slug'   => $slug,
		                                                      'fields' => array( 'sections' => false )
		) );

		$api[ $slug ] = false;

		if ( is_wp_error( $response ) ) {
			wp_die( esc_html( $this->strings['oops'] ) );
		} else {
			$api[ $slug ] = $response;
		}

//		}

		return $api[ $slug ];
	}

}