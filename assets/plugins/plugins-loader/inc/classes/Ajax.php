<?php
/**
 * Created by PhpStorm.
 * User: gerasart
 * Date: 7/26/2019
 * Time: 2:51 PM
 */

namespace PluginsLoader;


use PluginsLoader\helpers\AjaxHelper;
use PluginsLoader\helpers\Installer;
use PluginsLoader\Traits\PluginsHelper;

class Ajax extends AjaxHelper {

	use PluginsHelper;

	public function __construct() {
//	    update_option( 'current_plugins', [] );
		self::declaration_ajax();
	}

	public static function resultByType( $type ) {
		switch ( $type ) {
			case 'plugin-exist':
				return [
					'type'    => 'warn',
					'message' => 'Plugin already exist!',
				];
			case 'plugin-rejected':
				return [
					'type'    => 'error',
					'message' => 'Plugin not installed!',
				];
			case 'plugin-installed':
				return [
					'type'    => 'success',
					'message' => 'Plugin success installed!',
				];
			case 'plugin-activated':
				return [
					'type'    => 'success',
					'message' => 'Plugin success activated!',
				];
		}
	}

	public static function ajax_nopriv_setPlugins() {
		$data = $_POST['data'];

//        update_option( 'current_plugins', $data );
		ob_start();
		$installer = new Installer();
		$results   = $installer->installPlugins( $data[1]['items'] );
		ob_end_clean();

		wp_send_json_success( $results );
	}

	public static function ajax_installPlugin() {
		$plugin = $_POST['plugin'];

		ob_start();
//		ob_start('__return_empty_string', null, null);

		$installer = new Installer();
		$request   = $installer->install( $plugin );
		ob_get_clean();

		$result = self::resultByType( $request );
		$result['plugin'] = $plugin['name'];

		wp_send_json_success( $result );
    }

	public static function ajax_deactivatePlugins() {
		$plugins = $_POST['plugins'];

		$installer = new Installer();
		$result    = $installer->deactivatePlugins( $plugins );

		wp_send_json_success();
	}
}