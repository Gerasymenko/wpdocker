<?php


namespace PluginsLoader\Traits;


trait PluginsHelper {

	public static function getPluginsList() {
		$activated   = \get_option( 'active_plugins' );
		$all_plugins = \get_plugins();

//		var_dump($all_plugins);

		$plugins = [
			'active'   => [],
			'inactive' => [],
			'all' => [],
		];

		foreach ( $all_plugins as $key => $plugin ) {
			if ( dirname($key) !== 'plugins-loader' ) {
				$card = [
					'name' => $plugin['Name'],
					'slug' => dirname( $key ),
					'version' => $plugin['Version'],
				];

				if ( in_array( $key, $activated ) ) {
					$plugins['active'][] = $card;
				} else {
					$plugins['inactive'][] = $card;
				}

				$plugins['all'][] = $card;
			}
		}

		return $plugins;
	}

	public static function formatPluginList($list) {
	    $plugins = [];
		foreach ( $list as $item ) {
			$plugins[$item['slug']] = $item;
	    }
		return $plugins;
	}

}