<?php

namespace PluginsLoader\Traits;

trait Debugger {

	public static function debug($var) {
	    echo "<pre>";

	    var_dump($var);

	    echo "</pre>";
	}

}