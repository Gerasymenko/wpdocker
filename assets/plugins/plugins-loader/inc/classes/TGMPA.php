<?php
/**
 * Created by PhpStorm.
 * User: skipin
 * Date: 19.04.18
 * Time: 10:33
 */

namespace PluginsLoader;

class TGMPA {

    public function __construct() {
        add_action( 'tgmpa_register', array( __CLASS__, 'themeRecommendPlugin' ) );
    }

    public static function themeRecommendPlugin() {

        $plugins = Helpers::getPlugins();
        if ( $plugins ) {
            tgmpa( $plugins );
        }
    }

}
//var_dump(Helpers::getPlugins());