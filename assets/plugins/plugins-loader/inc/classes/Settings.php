<?php
/**
 * Created by PhpStorm.
 * User: gerasart
 * Date: 7/25/2019
 * Time: 6:13 PM
 */

namespace PluginsLoader;

use PluginsLoader\helpers\TemplateLoader;
use PluginsLoader\Traits\Debugger;
use PluginsLoader\Traits\PluginsHelper;

class Settings {

	use Debugger;
	use PluginsHelper;

	public function __construct() {
		add_action( 'admin_enqueue_scripts', array( $this, 'init_vars' ) );

		add_action( 'admin_menu', [ $this, 'setting_page' ] );
		add_action( "admin_menu", [ $this, 'display_theme_panel_fields' ] );
	}

	public function init_vars() {
		$args = [
			'installed_plugins' => self::getPluginsList(),
		];

		TemplateLoader::localizeArgs($args, 'jquery');
	}


	public function setting_page() {
		add_submenu_page(
			'plugins.php',
			'Plugins Loader',
			'Plugins Loader',
			'manage_options',
			'plugins_loader',
			[ $this, 'plugins_loader' ]
		);
	}


	public function display_theme_panel_fields() {
		register_setting( "section", "current_plugins" );
	}

	public function plugins_loader() {
		$rel_path = 'inc/classes/';

//		$plugins = self::getPluginsList();
//		self::debug( $plugins );
//
//		include SP_PATH . 'views/index.php';
//		$args = [
//			'installed_plugins' => $plugins,
//		];

		TemplateLoader::render_template_part('views/loader', null, [],
//			true, $rel_path, 'plugins-loader');
			true, $rel_path, 'jquery');
	}



}