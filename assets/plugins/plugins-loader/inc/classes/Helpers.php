<?php
/**
 * Created by PhpStorm.
 * User: gerasart
 * Date: 7/18/2019
 * Time: 10:28 AM
 */

namespace PluginsLoader;


class Helpers {


    public static function getPlugins() {
        $items = get_option( 'current_plugins' );
        $res = json_decode( json_encode( $items ) );
        $arr = array();
        if ( $res ) {
            foreach ( $res as $item ) {
                $arr[] = [
                    "name"   => $item->name,
                    "slug"   => $item->slug,
                    "source" => isset( $item->source ) && !empty($item->source) ? SP_PATH.$item->source : '',
                    "version" > $item->version
                ];
            }
            return $arr;
        }
    }
}