<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" href="@php echo get_template_directory_uri().'/assets/img/favicon.png' @endphp" />
  @php wp_head() @endphp
</head>
