# Edit conf file

* \docker\bin - .env

* MYSQL_ROOT_PASSWORD=admin
* PROJECT_NAME=creditnow
* MYSQL_DATABASE=creditnow
* MYSQL_USER=creditnow
* MYSQL_PASSWORD=creditnow

# http://portainer.wp.docker.localhost:8000

# http://pma.wp.docker.localhost:8000


# Work in fierfox

C:\Windows\System32\drivers\etc

нужно добавить домен для видимости

127.0.0.1 wp.docker.localhost


## Setup project
* Клонируем репозиторий docker-clear.
* Заходим в папку /docker/ и правим .env файл.
* Запускаем в папке /docker/bin/ bash-script restart.sh, который запускает docker-compose.
* С папки /assets/config/ копируем в папку /wp-app/ с заменой папку и файл wp-config.php
* Заходим в папку /wp-content/themes/ и запускаем команду в консоли:   composer create-project roots/sage   your-theme-name
* Следуем инструкциям в подсказках.
* Копируем все с папки /assets/theme/ в папку созданной темы с заменой.
* Копируем из /assets/composer.json все внутри разделов psr-4 и require-dev, и добавляем их в соответствующие разделы composer.json в папке темы.
* Запускаем в папке темы npm i и composer install  
* Запускаем npm скрипт командой npm run development   
* Откроется окно браузера и адрес http://wp.docker.localhost:8000/  
* Необходимо выполнить установку Wordpress.
* После установки, заходим в админку: удаляем стартовые плагины, переключаем на созданную нами тему и удаляем стартовые темы.
* Добавить код  require_once '__autoload.php'; в файл /resources/functions.php в папке темы.
* В самом конце удалить папку /assets/ и /.git/ (что бы можно было подключить другой репозиторий).
* Запускаем export_db.sh для создания бекапа базы.
* Делаем git init, git remote add origin repository-url, git add --all, git commit -am "Setup project", git push -u origin --all 
* Вводим в консоли git checkout -b dev, git push


## Дополнительно
**Стандартный логин и пароль**
* admin
* Aa1234567_

