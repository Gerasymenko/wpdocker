## Setup project
* Заходим в папку **/docker/** и правим **.env** файл.
* Запускаем в папке **/docker/bin/** bash-script **restart.sh**, который запускает docker-compose
* С папки **/assets/config/** копируем в папку **/wp-app/** с заменой папку и файл wp-config
* Заходим в папку **/wp-content/themes/** и запускаем команду в консоле:       **composer    create-project   roots/sage   theme_name**
* Копируем все с папки **/assets/theme/** в папку созданной темы с заменой
* Копируем все с папки **/assets/plugins/** и заменяем этой папкой папку **plugins**
* Удаляем папку созданной темы файл **.gitignore**
* Запускаем в папке темы npm i и composer install  
* Запускаем npm скрипт командой **npm run development**
* Добавить код из **/assets/theme/resources/functions.php** в файл **/resources/functions.php** в папке темы.
* В самом конце удалить папку **/assets/** и **/.git/** (что бы можно было подключить другой репозиторий).