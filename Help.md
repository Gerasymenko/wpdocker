**Sage**

- $acf_options - show vars from options page


**Blade front directives**

- @hierarchy
- @translate()
- @content
- @thePost
- @reset
- @dump()
- @php @endphp
- @if() @elseif() @else @endif 

**Helpers**
{!! App::title() !!}
\Theme\Help::Menu('name_menu')
@php wpm_language_switcher ('dropdown', 'name'); @endphp
wpm_translate_string()

**Server permissions**

chown -R systemd-timesync:systemd-journal ./*
chown -R www-data:www-data ./*
